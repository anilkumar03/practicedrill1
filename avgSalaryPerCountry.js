const employesData = require('./employesData');

let result = employesData.reduce((avgSalariesPerCountry,eachEmploye) =>{
    
    if(!avgSalariesPerCountry[eachEmploye.location]){
        avgSalariesPerCountry[eachEmploye.location] = {"count":0, "earning":0, "avg":0}
    }
    avgSalariesPerCountry[eachEmploye.location].count +=  1
    avgSalariesPerCountry[eachEmploye.location].earning += parseFloat(eachEmploye.salary.slice(1))
    avgSalariesPerCountry[eachEmploye.location].avg = (avgSalariesPerCountry[eachEmploye.location]['earning'])/(avgSalariesPerCountry[eachEmploye.location]['count'])
    return avgSalariesPerCountry
},{})

let answer  = {}
for(key in result){
    if(!answer[key]){
        answer[key] = result[key].avg
    }
}
console.log(answer)