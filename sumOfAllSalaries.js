const employesData = require('./employesData');

let result = employesData.reduce( (salariesSum,eachEmployee)=>{
    salariesSum += parseFloat((eachEmployee.salary).slice(1))
    return salariesSum
},0)

console.log(result.toFixed(3))