const employesData = require('./employesData');

let result = employesData.reduce((salariesSumPerCountry,eachEmploye) => {
    if(!salariesSumPerCountry[eachEmploye.location]){
        salariesSumPerCountry[eachEmploye.location] = 0
    }
    salariesSumPerCountry[eachEmploye.location] += parseFloat((eachEmploye.salary).slice(1))
    return salariesSumPerCountry
},{})

console.log(result)